#!/bin/bash

# This program adds a C++ file to a latex template
# document, compiles it and copies it out.

progName=$(ls | grep .cpp | awk -F '.' '{print $1}')      # Finds the cpp file name.
								  # Creates a latex document
                                                                  #   from a template:

mkLatex(){
    cat << EOF > ./latex/"$progName".tex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\usepackage[margin=0.5in]{geometry}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
    language=C++,
    aboveskip=3mm,
    belowskip=3mm,
    showstringspaces=false,
    columns=flexible,
    basicstyle={\small\ttfamily},
    numbers=none,
    numberstyle=\tiny\color{gray},
    keywordstyle=\color{blue},
    commentstyle=\color{dkgreen},
    stringstyle=\color{mauve},
    breaklines=true,
    breakatwhitespace=true,
    tabsize=3
    extendedchars=true,
    literate={å}{{\aa}}1 {æ}{{\ae}}1 {ø}{{\o}}1 {Å}{{\AA}}1 {Æ}{{\AE}}1 {Ø}{{\O}}1,
}
\begin{document}

\begin{lstlisting}
EOF
    cat "$progName".cpp >> ./latex/"$progName".tex        # Appends the C++ file to doc.

    cat << EOF >> ./latex/"$progName".tex			  # Ends the latex document.
\end{lstlisting}
\end{document}

EOF
}

if [ -z $progName  ]; then
    echo "C++ file not found."
else
    while :; do
	if [ -e ./latex ]; then
	    echo "running mklatex"
	    mkLatex
	    cd ./latex                                                      # Changes working directory to
	    latexmk -pdf                                                    #   latex document and compiles
	    exit
	else
	    echo "creating latex/"
	    mkdir latex
	fi
    done
fi
                                                                

