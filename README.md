# cpp-latex

Script to copy a cpp document into a LaTeX template and compile as pdf.

This is a standalone version of the script found in my other project [Task Create](https://gitlab.com/R0flcopt3r/task-create). The code is very messy and should be used with care.

## Usage

Run the script in the working directory with the cpp code.

## Dependencies

- latexmk
- cat

